# Quality Hound Webpage

This webpage displays Scratch code smells leveraging AngularJS as the main web framework. It consumes the analysis results from a web service (a separate repository)

## Setup
Needs a server to serve the html and resources to work properly:
For example:

Using a Python server:

```sh
cd q4blocks-qualityhound	#outside quality4blocks
python -m SimpleHTTPServer 1313
```

then visit http://localhost:1313/quality4blocks/
